package org.altbeacon.beaconreference

import android.annotation.SuppressLint
import android.app.*
import android.bluetooth.*
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.lifecycle.Observer
import org.altbeacon.beacon.*
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.URL
import java.time.Duration
import java.time.LocalDateTime
import java.util.*


class BeaconReferenceApplication : Application() {

    var handler: Handler? = null

    private val openTimeoutMinutes: Int = 10
    private var lastOpen: LocalDateTime? = null

    var mNotibuilder: Notification.Builder? = null

    var myBleGatt: BluetoothGatt? = null
    var myBleCharacteristic: BluetoothGattCharacteristic? = null

    var myBeacon: Beacon? = null
    var myBeaconTransmitter: BeaconTransmitter? = null

    lateinit var region: Region

    private var lastWifiDisconnect: LocalDateTime? = null
    private var lastWifiSsid: String = "none"
    private var wifiMinutesTimeout: Int = 5

    private val homeSsid: String = "infernet_doma"
    private val lat: Double = 49.8335714
    private val lon: Double = 18.1866789
    private val radius: Float = 100F

    private val geofencePendingIntent: PendingIntent by lazy {
        /*val geoRes: ResultReceiver = object : ResultReceiver(Handler(this.mainLooper)) {
            override fun onReceiveResult(resultCode: Int, resultData: Bundle)
            {
                Log.d(TAG, "aaaa geofence geo res: " + resultData.toString())
                webLog("geo res: " + resultData.toString())
            }
        }*/

        val geoRes = GeofenceResultReceiver(Handler(this.mainLooper))
        geoRes.setReceiver(GeoResultReceiverCallBack(this))

        val b = Bundle()
        b.putParcelable("myrec", geoRes)

        val intent = Intent(this, GeofenceBroadcastReceiver::class.java)
        intent.putExtra("myrec", b)

        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        PendingIntent.getBroadcast(
            this,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT + PendingIntent.FLAG_MUTABLE
        )
    }

    private class GeoResultReceiverCallBack(app: BeaconReferenceApplication) :
        GeofenceResultReceiver.ResultReceiverCallBack {

        private val appRef: WeakReference<BeaconReferenceApplication>?

        init {
            appRef = WeakReference(app)
        }

        override fun onSuccess(res: String) {
            Log.d(TAG, "aaaa geofence geo res: " + res)
            appRef?.get()?.webLog("geo res: " + res)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onCreate() {
        super.onCreate()

        handler = Handler(Looper.getMainLooper())


        val beaconManager = BeaconManager.getInstanceForApplication(this)
        //BeaconManager.setDebug(true)
        /*
                val geofencingClient = LocationServices.getGeofencingClient(this)
                val geofence = Geofence.Builder()
                    .setRequestId("mygeo123")
                    .setCircularRegion(lat, lon, radius)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setLoiteringDelay(10000)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_EXIT or Geofence.GEOFENCE_TRANSITION_DWELL)
                    .build()

                val geofencingRequest = GeofencingRequest.Builder()
                    .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                    .addGeofence(geofence)
                    .build()

                geofencingClient.addGeofences(geofencingRequest, geofencePendingIntent).run {
                    addOnSuccessListener {
                        Log.d(TAG, "aaaa onSuccess: geo")
                    }
                    addOnFailureListener {
                        Log.d(TAG, "aaaa onFailure: geo")
                    }
                }
        */
        // --------------

        val connec = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val wifiManager = getSystemService(Context.WIFI_SERVICE) as WifiManager

        val networkRequestWiFi = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .removeCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build()


        connec.registerNetworkCallback(
            networkRequestWiFi,
            object : ConnectivityManager.NetworkCallback() {
                override fun onLost(network: Network) {

                    Log.d(TAG, "aaaa outside wifi")
                    webLog("wifi: disconnect " + lastWifiSsid)

                    //if (lastWifiSsid == homeSsid) {
                    lastWifiDisconnect = LocalDateTime.now()

                    Log.d(TAG, "aaaa start scan")

                    beaconManager.setBackgroundBetweenScanPeriod(0)
                    beaconManager.updateScanPeriods()

                    //beaconManager.startMonitoring(region)
                    //beaconManager.startRangingBeacons(region)
                    webLog("ble: start scan")

                    var m = ""
                    if (myBeaconTransmitter != null) {
                        myBeaconTransmitter?.startAdvertising(
                            myBeacon,
                            object : AdvertiseCallback() {
                                override fun onStartFailure(errorCode: Int) {
                                    Log.e(
                                        TAG,
                                        "Advertisement start failed with code: $errorCode"
                                    )
                                }

                                override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
                                    Log.i(TAG, "Advertisement start succeeded.")
                                }
                            })

                        m = " & advertisement"
                    }

                    //} else {
                    //    lastWifiDisconnect = null
                    //}

                    changeNotifyTitle("wifi lost - start scan$m")
                }

                override fun onAvailable(network: Network) {

                    //val info: WifiInfo? = wifiManager.getConnectionInfo()

                    Log.d(TAG, "aaaa inside wifi "/* + info?.getSSID()*/)
                    webLog("wifi: connect "/*+ info?.getSSID()*/)

                    //lastWifiSsid = info?.getSSID()?.trim('"')
                    lastWifiDisconnect = null

                    //if (lastWifiSsid == homeSsid) {
                    Log.d(TAG, "aaaa stop scan")


                    beaconManager.setBackgroundBetweenScanPeriod(360000000L)
                    beaconManager.updateScanPeriods()

                    //beaconManager.stopMonitoring(region)
                    //beaconManager.stopRangingBeacons(region)

                    webLog("ble: stop scan")

                    var m = ""
                    if (myBeaconTransmitter != null) {
                        myBeaconTransmitter?.stopAdvertising()
                        m = " & advertisement"
                    }

                    //}

                    changeNotifyTitle("wifi in - stop scan$m")
                }
            })

        // -----


        // By default the AndroidBeaconLibrary will only find AltBeacons.  If you wish to make it
        // find a different type of beacon, you must specify the byte layout for that beacon's
        // advertisement with a line like below.  The example shows how to find a beacon with the
        // same byte layout as AltBeacon but with a beaconTypeCode of 0xaabb.  To find the proper
        // layout expression for other beacon types, do a web search for "setBeaconLayout"
        // including the quotes.
        //
        // ALTBEACON      m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25
        // EDDYSTONE TLM  x,s:0-1=feaa,m:2-2=20,d:3-3,d:4-5,d:6-7,d:8-11,d:12-15
        // EDDYSTONE UID  s:0-1=feaa,m:2-2=00,p:3-3:-41,i:4-13,i:14-19
        // EDDYSTONE URL  s:0-1=feaa,m:2-2=10,p:3-3:-41,i:4-20v
        // IBEACON        m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24
        //
        //beaconManager.getBeaconParsers().clear();
        //beaconManager.getBeaconParsers().add(new BeaconParser().
        //        setBeaconLayout("m:0-1=4c00,i:2-24v,p:24-24"));


        // By default the AndroidBeaconLibrary will only find AltBeacons.  If you wish to make it
        // find a different type of beacon like Eddystone or iBeacon, you must specify the byte layout
        // for that beacon's advertisement with a line like below.
        //
        // If you don't care about AltBeacon, you can clear it from the defaults:
        beaconManager.getBeaconParsers().clear()

        // The example shows how to find iBeacon.
        beaconManager.getBeaconParsers().add(
            BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24")
        )

        // enabling debugging will send lots of verbose debug information from the library to Logcat
        // this is useful for troubleshooting problmes
        // BeaconManager.setDebug(true)


        // The BluetoothMedic code here, if included, will watch for problems with the bluetooth
        // stack and optionally:
        // - power cycle bluetooth to recover on bluetooth problems
        // - periodically do a proactive scan or transmission to verify the bluetooth stack is OK
        //BluetoothMedic.getInstance().enablePowerCycleOnFailures(this)
        //BluetoothMedic.getInstance().enablePeriodicTests(this, BluetoothMedic.SCAN_TEST + BluetoothMedic.TRANSMIT_TEST)

        // By default, the library will scan in the background every 5 minutes on Android 4-7,
        // which will be limited to scan jobs scheduled every ~15 minutes on Android 8+
        // If you want more frequent scanning (requires a foreground service on Android 8+),
        // configure that here.
        // If you want to continuously range beacons in the background more often than every 15 mintues,
        // you can use the library's built-in foreground service to unlock this behavior on Android
        // 8+.   the method below shows how you set that up.
        setupForegroundService()
        beaconManager.setEnableScheduledScanJobs(false)
        beaconManager.setBackgroundBetweenScanPeriod(0)
        beaconManager.setBackgroundScanPeriod(1100)

        // Ranging callbacks will drop out if no beacons are detected
        // Monitoring callbacks will be delayed by up to 25 minutes on region exit
        // beaconManager.setIntentScanningStrategyEnabled(true)

        // The code below will start "monitoring" for beacons matching the region definition below
        // the region definition is a wildcard that matches all beacons regardless of identifiers.
        // if you only want to detect beacons with a specific UUID, change the id1 paremeter to
        // a UUID like Identifier.parse("2F234454-CF6D-4A0F-ADF2-F4911BA9FFA6")
        region = Region(
            "all-beacons",
            Identifier.parse("B2B98DE4-C81C-47C2-B14E-791B3E5587EC"),
            null,
            null
        )
        beaconManager.startMonitoring(region)
        //beaconManager.startRangingBeacons(region)


        // These two lines set up a Live Data observer so this Activity can get beacon data from the Application class
        val regionViewModel =
            BeaconManager.getInstanceForApplication(this).getRegionViewModel(region)
        // observer will be called each time the monitored regionState changes (inside vs. outside region)
        regionViewModel.regionState.observeForever(centralMonitoringObserver)
        // observer will be called each time a new list of beacons is ranged (typically ~1 second in the foreground)
        //regionViewModel.rangedBeacons.observeForever(centralRangingObserver)

        if (Build.MODEL == "SM-G973F") {
            initTransmitBeacon()
        }

        //sendBleCmd()


        // TODO:
        // startuju scaning podle geofence - jsem blizko bytu
        //   - vstup do zony - zacinam skenovat
        //   - vystup ze zony - stop sannng, pokud bezi
        //   - najdu beacon - oteviram
        //   - zrusim skenovani
        //
        // jak vyresit napr. prochazku s kosem?
        //   - po odpojeni z domaci wifi, zacinam skenovat
        //   - skenovani rusim vystupem z geo zony nebo pripojenim na wifi
    }

    @SuppressLint("MissingPermission")
    fun sendBleCmd() {

        val bluetoothGattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {
            override fun onConnectionStateChange(
                gatt: BluetoothGatt, status: Int,
                newState: Int
            ) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    Log.i(TAG, "Connected to GATT server.");
                    gatt.discoverServices();
                }
            }

            override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    val services = gatt.services
                    for (service in services) {
                        val characteristics = service.characteristics
                        for (characteristic in characteristics) {
                            myBleCharacteristic = characteristic
                            sendBleCmd()
                        }
                    }
                }
            }
        }

        val that = this
        val scanCallback = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult) {
                super.onScanResult(callbackType, result)
                Log.i(TAG, "eeee Remote device name: " + result.getDevice())

                myBleGatt = result.getDevice().connectGatt(that, true, bluetoothGattCallback);
            }
        }


        if (myBleCharacteristic == null && myBleGatt == null) {
            val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            val bluetoothAdapter = bluetoothManager.getAdapter()

            val mHandler = Handler()
            mHandler.postDelayed(Runnable {
                bluetoothAdapter.bluetoothLeScanner.stopScan(scanCallback)
            }, 10)

            bluetoothAdapter.bluetoothLeScanner.startScan(scanCallback)
        } else {
            myBleCharacteristic?.setValue("test")
            myBleCharacteristic?.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
            myBleGatt?.writeCharacteristic(myBleCharacteristic);
        }
    }

    fun initTransmitBeacon() {

        var beaconId = "6b28cdea-dbf0-499e-9b4f-7114ea145dba"
        if (Build.MODEL == "SM-G973F") {
            beaconId = "6b28cdea-dbf0-499e-9b4f-7114ea145dbe"
        }


        myBeacon = Beacon.Builder()
            .setId1(beaconId)
            .setId2("1")
            .setId3("2")
            //.setManufacturer(0x0118) // Radius Networks.  Change this for other beacon layouts
            .setManufacturer(0x004c)
            .setTxPower(-59)
            //.setDataFields(Arrays.asList(arrayOf(0L))) // Remove this for beacon layouts without d: fields
            .build()

        // Change the layout below for other beacon types

        // Change the layout below for other beacon types
        val beaconParser = BeaconParser()
            //.setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25")
            //.setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24")
            .setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24")

        myBeaconTransmitter = BeaconTransmitter(applicationContext, beaconParser)
        //myBeaconTransmitter?.advertiseMode = AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY
        myBeaconTransmitter?.startAdvertising(myBeacon, object : AdvertiseCallback() {
            override fun onStartFailure(errorCode: Int) {
                Log.e(TAG, "Advertisement start failed with code: $errorCode")
            }

            override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
                Log.i(TAG, "Advertisement start succeeded.")
            }
        })
    }

    fun setupForegroundService() {

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val channel = NotificationChannel(
            "beacon-ref-notification-id",
            "Background scaning", NotificationManager.IMPORTANCE_DEFAULT
        )

        notificationManager.createNotificationChannel(channel)

        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT + PendingIntent.FLAG_IMMUTABLE
        )

        mNotibuilder = Notification.Builder(this, channel.id)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentTitle("Scanning for Beacons")
            .setContentIntent(pendingIntent)

        BeaconManager.getInstanceForApplication(this)
            .enableForegroundServiceScanning(mNotibuilder?.build(), 456)
    }

    private fun changeNotifyTitle(msg: String) {
        if (mNotibuilder == null) {
            return
        }

        mNotibuilder?.setContentTitle(msg)

        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(456, mNotibuilder?.build());
    }

    fun cancelNotification() {
        handler?.removeCallbacksAndMessages(null)

        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(789)
    }

    fun sendNotification() {
        val intent = Intent(this, CmdActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra("cmd", "open");

        val resultPendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_CANCEL_CURRENT + PendingIntent.FLAG_IMMUTABLE
        )

        /*val stackBuilder = TaskStackBuilder.create(this)
        stackBuilder.addNextIntent(intent)
        val resultPendingIntent = stackBuilder.getPendingIntent(
            0,
            PendingIntent.FLAG_UPDATE_CURRENT + PendingIntent.FLAG_IMMUTABLE
        )*/


        val channel = NotificationChannel(
            "beacon-ref-notification-id2",
            "Notification", NotificationManager.IMPORTANCE_DEFAULT
        )

        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)

        val builder = NotificationCompat.Builder(this, channel.id)
            .setContentTitle("Opening the front door..")
            .setContentText("Click to open again.")
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentIntent(resultPendingIntent)

        notificationManager.notify(789, builder.build())

        handler?.postDelayed({
            notificationManager.cancel(789)
        }, openTimeoutMinutes * 60 * 1000L)
    }

    private val centralMonitoringObserver = Observer<Int> { state ->
        if (state == MonitorNotifier.OUTSIDE) {
            webLog("ble: outside")
            Log.d(TAG, "aaaa outside beacon region: " + region)
        } else {
            webLog("ble: inside")
            Log.d(TAG, "aaaa inside beacon region: " + region)

            if (lastWifiDisconnect == null || Duration.between(
                    lastWifiDisconnect,
                    LocalDateTime.now()
                ).toMinutes() > wifiMinutesTimeout
            ) {

                if (lastOpen == null || Duration.between(lastOpen, LocalDateTime.now())
                        .toMinutes() > openTimeoutMinutes
                ) {
                    open()
                    sendNotification()

                    lastOpen = LocalDateTime.now()

                    webLog("nuki: opening")
                    Log.d(TAG, "aaaa opening..")
                } else {
                    webLog("nuki: skip opening")
                    Log.d(TAG, "INFO nuki: skip opening")
                }
            } else {
                webLog("nuki: not open, wifi timeout")
                Log.d(TAG, "aaaa not opening, wifi timeout..")
                lastWifiDisconnect = null
            }
        }
    }

    val centralRangingObserver = Observer<Collection<Beacon>> { beacons ->
        Log.d(MainActivity.TAG, "aaaa Ranged: ${beacons.count()} beacons")
        for (beacon: Beacon in beacons) {
            //Log.d(TAG, "$beacon about ${beacon.distance} meters away aaaa")
            Log.d(TAG, "$beacon RSSI: ${beacon.rssi} aaaa")
            if (beacon.rssi > -70) {

            }


        }
    }

    fun webLog(msg: String) {
        Log.d(TAG, "INFO webLog: " + msg)
        Thread {
            try {
                val endpoint = URL("http://florbal.criticalsolution.cz/api/log?msg=" + msg)
                val myConnection = endpoint.openConnection() as HttpURLConnection
                if (myConnection.responseCode != 200) {
                    Log.d(TAG, "ERROR webLog: code:" + myConnection.responseCode)
                }

                myConnection.disconnect()
            } catch (e: Exception) {
                Log.d(TAG, "ERROR webLog: " + e.message + e.javaClass)
            }
        }.start()
    }

    fun open() {
        Log.d(TAG, "INFO nuki: open")
        Thread {
            try {
                val endpoint = URL("https://api.nuki.io/smartlock/9099514290/action/unlock")
                val myConnection = endpoint.openConnection() as HttpURLConnection

                myConnection.requestMethod = "POST"
                myConnection.setRequestProperty("Accept", "application/json")
                myConnection.setRequestProperty(
                    "Authorization",
                    "Bearer a20573e42dcb1ca88e937aa6a5a0d031229e7311cff88b8792d66e69ef8e64b0976362862eb53877"
                )

                if (myConnection.responseCode != 200 || myConnection.responseCode != 204) {
                    Log.d(TAG, "ERROR nuki: code:" + myConnection.responseCode)
                }

                myConnection.disconnect()


            } catch (e: Exception) {
                Log.d(TAG, "ERROR nuki: " + e.message + e.javaClass)
            }
        }.start()
    }

    companion object {
        val TAG = "BeaconReference"
    }
}