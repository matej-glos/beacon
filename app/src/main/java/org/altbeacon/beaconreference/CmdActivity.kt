package org.altbeacon.beaconreference

import android.app.Activity
import android.os.Bundle



class CmdActivity : Activity() {

    lateinit var beaconReferenceApplication: BeaconReferenceApplication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        beaconReferenceApplication = application as BeaconReferenceApplication


        if (intent.hasExtra("cmd"))
        {
            val cmd = intent.getStringExtra("cmd")
            if (cmd == "open") {
                beaconReferenceApplication.cancelNotification()
                beaconReferenceApplication.open()
                beaconReferenceApplication.sendNotification()
            }
        }

        finish()
    }
}