package org.altbeacon.beaconreference

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import android.util.Log
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent


/**
 * Receiver for geofence transition changes.
 *
 *
 * Receives geofence transition events from Location Services in the form of an Intent containing
 * the transition type and geofence id(s) that triggered the transition. Creates a JobIntentService
 * that will handle the intent in the background.
 */
class GeofenceBroadcastReceiver : BroadcastReceiver() {

    /**
     * Receives incoming intents.
     *
     * @param context the application context.
     * @param intent  sent by Location Services. This Intent is provided to Location
     * Services (inside a PendingIntent) when addGeofences() is called.
     */
    override fun onReceive(context: Context, intent: Intent) {
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        if (geofencingEvent == null || geofencingEvent.hasError()) {
            Log.e(TAG, "geofence error..")
            return
        }

        //val resultReceiver: ResultReceiver? = intent.getParcelableExtra("myrec")
        val b: Bundle? = intent.getBundleExtra("myrec")
        val resultReceiver: ResultReceiver? = b?.getParcelable("myrec")


        // Get the transition type.
        val geofenceTransition = geofencingEvent.geofenceTransition

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            Log.i(TAG, "aaaa geofence enter")

            val bundle = Bundle()
            bundle.putString("res", "enter")
            resultReceiver?.send(0, bundle)
        } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            Log.i(TAG, "aaaa geofence exit")

            val bundle = Bundle()
            bundle.putString("res", "exit")
            resultReceiver?.send(0, bundle)
        } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL) {
            Log.i(TAG, "aaaa geofence dwell")

            val bundle = Bundle()
            bundle.putString("res", "dwell")
            resultReceiver?.send(0, bundle)
        }
    }

    companion object {
        val TAG = "GeofenceBroadcastReceiver"
    }
}