package org.altbeacon.beaconreference

import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver

class GeofenceResultReceiver(handler: Handler?) : ResultReceiver(handler) {
    private var mReceiver: ResultReceiverCallBack? = null
    fun setReceiver(receiver: ResultReceiverCallBack?) {
        mReceiver = receiver
    }

    override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
        if (mReceiver != null) {
            val res = resultData.getString("res")
            if (res != null) {
                mReceiver!!.onSuccess(res)
            }
        }
    }

    interface ResultReceiverCallBack {
        fun onSuccess(res: String)
    }

}